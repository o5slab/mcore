# mcore

[![pipeline status](https://gitlab.com/o5slab/mcore/badges/master/pipeline.svg)](https://gitlab.com/o5slab/mcore/-/commits/master)
[![coverage report](https://gitlab.com/o5slab/mcore/badges/master/coverage.svg)](https://gitlab.com/o5slab/mcore/-/commits/master)

A microservices toolbox that holds some utilities for bulding microservices easilier.

## Components

- [Database](./db/)

## Contact

- [Phuong Huynh](mailto:phuonghuynh.net@gmail.com)
