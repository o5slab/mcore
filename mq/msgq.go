package mq

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// MsgQ ...
type MsgQ interface {
	AddListener(l *MsgListener) error
	RemoveListener(l *MsgListener)
	Connect() error
	Close()
	Start(ctx context.Context)
	Publish(topic string, m amqp.Publishing) error
}

// MsgListener ...
type MsgListener struct {
	Topic   string
	Queue   string
	Handler func(m amqp.Delivery)
	Tag     string

	closeCh chan struct{}
}

type msgQueueImpl struct {
	listeners    []*MsgListener
	conn         *amqp.Connection
	ch           *amqp.Channel
	exchangeName string
	url          string
	wg           sync.WaitGroup
	started      bool
	mux          sync.Mutex
}

// RemoveListener ...
func (mq *msgQueueImpl) RemoveListener(l *MsgListener) {
	if l == nil {
		return
	}

	_ = mq.ch.Cancel(l.Tag, false)
}

// Close the connection
func (mq *msgQueueImpl) Close() {
	_ = mq.conn.Close()
}

// Connect opens connection to broker
// declare default exchange for publishing as well
func (mq *msgQueueImpl) Connect() error {
	var err error
	dialConfig := amqp.Config{
		Heartbeat: time.Second * 30,
		Dial:      amqp.DefaultDial(time.Second * 5),
	}
	if mq.conn, err = amqp.DialConfig(mq.url, dialConfig); err != nil {
		return err
	}

	if mq.ch, err = mq.conn.Channel(); err != nil {
		return err
	}

	// declare exchange
	if mq.exchangeName == "" {
		mq.exchangeName = DefaultExchange
	}

	err = mq.ch.ExchangeDeclare(
		mq.exchangeName,
		amqp.ExchangeTopic,
		true,  // durable
		false, // auto delete
		false, // internal
		false, // no-wait
		nil,
	)
	if err != nil {
		return err
	}

	return nil
}

// Publish publish message
// in real life, we should use 2 MsgQ objects, 1 for listeners, 1 for publishing
func (mq *msgQueueImpl) Publish(topic string, m amqp.Publishing) error {
	if mq.conn == nil || mq.conn.IsClosed() {
		if err := mq.Connect(); err != nil {
			return err
		}
	}

	err := mq.ch.Publish(mq.exchangeName, topic, false, false, m)
	return err
}

// NewMsgQ ...
func NewMsgQ(url string) MsgQ {
	return &msgQueueImpl{url: url}
}

// AddListener add an listener
// if the consumer is started, this listener will consume to broker immediately
// otherwise consuming will be postpone to Start calling
func (mq *msgQueueImpl) AddListener(l *MsgListener) error {
	ctxlog := logrus.WithField("func", "msgQueueImpl.AddListener")

	mq.mux.Lock()
	isStarted := mq.started
	mq.listeners = append(mq.listeners, l)
	mq.mux.Unlock()

	if isStarted {
		ctxlog.Debug("consumer has started, subscribe this listener immediately")
		return mq.subscribeListener(l)
	}

	return nil
}

func (mq *msgQueueImpl) Start(ctx context.Context) {
	ctxlog := logrus.WithField("func", "MsgQ.Start")

	closeErrCh := make(chan *amqp.Error)
	startConn := make(chan struct{}, 1)
	startConn <- struct{}{}
	reconnectDelay := time.Second * 10

	for {
		select {
		case <-ctx.Done():
			ctxlog.Debug("close connection")
			if !mq.conn.IsClosed() {
				_ = mq.conn.Close()
			}
			mq.mux.Lock()
			mq.started = false
			mq.mux.Unlock()
			return

		case <-startConn:
			ctxlog.Debug("start connecting")
			if err := mq.subscribe(); err != nil {
				ctxlog.WithError(err).Error("error while connecting, reconnect latter")
				// sleep a bit before reconnect
				time.Sleep(reconnectDelay)
				startConn <- struct{}{}
				continue
			}
			mq.mux.Lock()
			mq.started = true
			mq.mux.Unlock()
			ctxlog.Debug("connected")
			// after subscribe sucessfully, set up close notify
			mq.conn.NotifyClose(closeErrCh)

		case err, abnormalClose := <-closeErrCh:
			if !abnormalClose {
				ctxlog.Debug("shutdown normally")
				return
			}
			ctxlog.WithError(err).Error("connection closed, reconnect latter")
			mq.mux.Lock()
			mq.started = false
			mq.mux.Unlock()
			time.Sleep(reconnectDelay)
			startConn <- struct{}{}
			closeErrCh = make(chan *amqp.Error)
			// will reconnect in next loop
		}
	}
}

func (mq *msgQueueImpl) subscribe() error {
	var err error
	if mq.conn == nil || mq.conn.IsClosed() {
		if err = mq.Connect(); err != nil {
			return err
		}
	}

	// subscriber to broker
	for _, listener := range mq.listeners {
		if err = mq.subscribeListener(listener); err != nil {
			return err
		}
	}

	return nil
}

func (mq *msgQueueImpl) subscribeListener(l *MsgListener) error {
	durable := len(l.Queue) > 0
	deleteOnUnused := !durable
	var (
		queue amqp.Queue
		err   error
	)
	if queue, err = mq.ch.QueueDeclare(l.Queue, durable, deleteOnUnused, false, false, nil); err != nil {
		return err
	}

	// bind queue to exchange
	if err = mq.ch.QueueBind(queue.Name, l.Topic, mq.exchangeName, false, nil); err != nil {
		return err
	}

	// make sure we have tag
	if l.Tag == "" {
		l.Tag = fmt.Sprintf("%s-%d", l.Topic, time.Now().Unix())
	}

	var msgCh <-chan amqp.Delivery
	if msgCh, err = mq.ch.Consume(queue.Name, l.Tag, false, false, false, false, nil); err != nil {
		return err
	}
	mq.wg.Add(1)
	go mq.startListener(l, msgCh)
	return nil
}

func (mq *msgQueueImpl) startListener(l *MsgListener, msgCh <-chan amqp.Delivery) {
	defer mq.wg.Done()

	for {
		select {
		case m, ok := <-msgCh:
			if !ok {
				// messages channel is closed
				return
			}

			// call handler
			l.Handler(m)

		case <-l.closeCh:
			return
		}
	}
}
