package gina

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// HealthResponse defines a structure for responding heath check
type HealthResponse struct {
	Name      string `json:"name"`
	Version   string `json:"version"`
	BuildTime string `json:"build_time"`
	Uptime    string `json:"uptime"`
	Env       string `json:"env"`
}

// NewHealthHandler make a gin compatible health check handler
// On requested, this just response name, version and how long did it run
// don't too much but helpful
func NewHealthHandler(name, version, buildTime, env string) gin.HandlerFunc {
	start := time.Now()
	baseRsp := HealthResponse{
		Name:      name,
		Version:   version,
		BuildTime: buildTime,
		Uptime:    "",
		Env:       env,
	}

	return func(c *gin.Context) {
		rsp := baseRsp
		rsp.Uptime = time.Now().Sub(start).String()
		c.JSON(http.StatusOK, rsp)
	}
}
