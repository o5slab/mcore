package gina

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// RequestID returns request id middleware
func RequestID(headerName string) gin.HandlerFunc {
	return func(c *gin.Context) {
		requestid := c.GetHeader(headerName)
		if requestid == "" {
			requestid = uuid.New().String()
			c.Header(headerName, requestid)
		}
		// set to context
		c.Set(headerName, requestid)

		c.Next()
	}
}
