# Gina

Some customized parts that are based on go gin framework.

- [Health check](#health-check)

---
## Health check

It is a handler function that responds a health check status. Just need to provide it your service name,
service version, and the build time as well (feel free to pass empty).

```go
server := gin.New()
server.Use(gina.NewHealthHandler("core-service", "v1.0.0", "2020-07-14"))
```

## Nice Failure Recovery

This customized recovery will help to response beauty response some panic cases like failed validation,
custom responses, etc. Dead simple example:

```

# init router
route.Use(gina.NiceFailureRecover())

# in a handler
func Handle(c *gin.Context) {
    panic(NewResponseError(400, "test error"))
}

```

Above snipet will produce response JSON body like:

```json
{"error": "test error"}
```

in http 400 status code

## Customized validation

As you already know, gin modified validator to use its own binding tags. But actually I needn't that,
I need `validate` tag (to reuse in other input for example).

to use this customization, place in `init` block of your application.

```go
import (
  "github.com/gin-gonic/gin/binding"
)

binding.Validator = gina.NewValidator()
```

thank for that, any context.ShouldBindJSON and related stuff will use our Validator instead of gin customized one.

