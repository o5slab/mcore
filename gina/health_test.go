package gina

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestHealthHandler(t *testing.T) {
	handler := NewHealthHandler("echo", "v1.0.0", time.Now().Format(time.RFC3339), "dev")
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	handler(ctx)
	assert.Equal(t, http.StatusOK, w.Code)

	r := HealthResponse{}
	err := json.NewDecoder(w.Body).Decode(&r)
	assert.NoError(t, err)
	assert.Equal(t, "echo", r.Name)
	assert.Equal(t, "v1.0.0", r.Version)
	assert.NotEmpty(t, r.Version)
	assert.Equal(t, "dev", r.Env)
}
