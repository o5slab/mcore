package gina

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

const (
	// AuthXHeader ...
	AuthXHeader = "x-user-id"
)

func TerminateOnError(fn func(c *gin.Context) error) gin.HandlerFunc {
	return func(c *gin.Context) {
		if err := fn(c); err != nil {
			c.Abort()
		}
	}
}

// GetReqAuth extract authorized user ID from header
func MustGetAuth(c *gin.Context) uuid.UUID {
	header := c.GetHeader(AuthXHeader)
	if header == "" {
		panic(errors.New("empty authx header"))
	}

	return uuid.MustParse(header)
}

// RequireAuth is middleware that breaks the process when missed authorization header
func RequireAuth(c *gin.Context) {
	header := c.GetHeader(AuthXHeader)
	if header != "" {
		if _, err := uuid.Parse(header); err == nil {
			c.Next()

			return
		}
	}
	c.AbortWithStatus(http.StatusUnauthorized)
}

// NiceFailureRecovery ...
func NiceFailureRecovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			var recoveredErr interface{}
			if recoveredErr = recover(); recoveredErr == nil {
				return
			}

			// configured error
			if errR, ok := recoveredErr.(ResponseError); ok {
				c.AbortWithStatusJSON(errR.GetStatusCode(), errR.GetErrorsMap())
				return
			}

			// handle validation error
			if errValidation, ok := recoveredErr.(ValidatorErrors); ok {
				c.AbortWithStatusJSON(http.StatusBadRequest, errValidation.GetErrorsMap())
				return
			}

			// handle single error with some specified cases like empty body (EOF)
			if errE, ok := recoveredErr.(error); ok {
				logrus.
					WithField("path", c.Request.URL.Path).
					WithField("method", c.Request.Method).
					WithError(errE).
					Warn("handle panic error")

				switch errE.Error() {
				case "EOF":
					c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid request body"})
					return

				default:
					c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": errE.Error()})
					return
				}
			}

			if errS, ok := recoveredErr.(string); ok {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": errS})
				return
			}
		}()

		c.Next()
	}
}
