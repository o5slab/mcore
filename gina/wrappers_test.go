package gina

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

func TestTerminateOnError(t *testing.T) {
	gin.SetMode(gin.TestMode)
	mainHandler := func(c *gin.Context) error {
		return errors.New("faked error")
	}
	handler := TerminateOnError(mainHandler)
	ctx, _ := gin.CreateTestContext(nil)
	handler(ctx)

	assert.True(t, ctx.IsAborted())
}

func TestMustGetAuth(t *testing.T) {
	gin.SetMode(gin.TestMode)
	ctx, _ := gin.CreateTestContext(nil)
	ctx.Request, _ = http.NewRequest(http.MethodGet, "/", nil)
	ctx.Request.Header.Set(AuthXHeader, "")

	assert.Panics(t, func() {
		MustGetAuth(ctx)
	})

	xuid := uuid.New().String()
	ctx.Request.Header.Set(AuthXHeader, xuid)
	gotID := MustGetAuth(ctx)
	assert.Equal(t, xuid, gotID.String())
}

func TestRequireAuthAborted(t *testing.T) {
	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request, _ = http.NewRequest(http.MethodGet, "/", nil)
	ctx.Request.Header.Set(AuthXHeader, "")
	RequireAuth(ctx)
	assert.True(t, ctx.IsAborted())
}

func TestRequireAuthPassThrough(t *testing.T) {
	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request, _ = http.NewRequest(http.MethodGet, "/", nil)
	ctx.Request.Header.Set(AuthXHeader, uuid.New().String())
	RequireAuth(ctx)
	assert.False(t, ctx.IsAborted())
}

type recoverTestSuite struct {
	suite.Suite

	engine  *gin.Engine
	recoder *httptest.ResponseRecorder
}

func (ts *recoverTestSuite) SetupTest() {
	gin.SetMode(gin.TestMode)
	ts.recoder = httptest.NewRecorder()
	_, ts.engine = gin.CreateTestContext(ts.recoder)

	ts.engine.Use(NiceFailureRecovery())
}

func (ts *recoverTestSuite) getRootRequest() *http.Request {
	req, _ := http.NewRequest("GET", "/", nil)
	return req
}

func (ts *recoverTestSuite) TestPassThrough() {
	handler := func(c *gin.Context) {
		// do nothing
	}
	ts.engine.GET("/", handler)
	ts.engine.ServeHTTP(ts.recoder, ts.getRootRequest())

	ts.Assert().Equal(200, ts.recoder.Code)
}

func (ts *recoverTestSuite) TestAbortOnWellKnowErrors() {
	raiseErrorHandler := func(c *gin.Context) {
		panic(NewResponseError(400, "test error"))
	}

	ts.engine.GET("/", raiseErrorHandler)
	ts.engine.ServeHTTP(ts.recoder, ts.getRootRequest())

	ts.Assert().Equal(400, ts.recoder.Code)

	var payload map[string]interface{}
	json.NewDecoder(ts.recoder.Body).Decode(&payload)
	ts.Assert().Equal("test error", payload["error"].(string))
}

func (ts *recoverTestSuite) TestAbortOnValidationErrors() {
	raiseErrorHandler := func(c *gin.Context) {
		err := &validationErrors{
			fieldErrors: []ValidatorFieldError{},
		}
		err.fieldErrors = append(err.fieldErrors, &validatorFieldError{
			field:   "username",
			message: "must not be empty",
		})

		panic(err)
	}

	ts.engine.GET("/", raiseErrorHandler)
	ts.engine.ServeHTTP(ts.recoder, ts.getRootRequest())

	ts.Assert().Equal(400, ts.recoder.Code)

	var payload map[string]interface{}
	json.NewDecoder(ts.recoder.Body).Decode(&payload)
	ts.Assert().Equal("must not be empty", payload["username"].(string))
}

func (ts *recoverTestSuite) TestWellknowText() {
	handler := func(c *gin.Context) {
		panic(errors.New("EOF"))
	}
	ts.engine.GET("/", handler)
	ts.engine.ServeHTTP(ts.recoder, ts.getRootRequest())

	ts.Assert().Equal(400, ts.recoder.Code)
}

func (ts *recoverTestSuite) TestUnknownError() {
	handler := func(c *gin.Context) {
		panic(errors.New("unknown error"))
	}
	ts.engine.GET("/", handler)
	ts.engine.ServeHTTP(ts.recoder, ts.getRootRequest())

	ts.Assert().Equal(400, ts.recoder.Code)
	var payload map[string]interface{}
	json.NewDecoder(ts.recoder.Body).Decode(&payload)
	ts.Assert().Equal("unknown error", payload["error"].(string))
}

func (ts *recoverTestSuite) TestSimpleTextPanic() {
	handler := func(c *gin.Context) {
		panic("plain text panic")
	}
	ts.engine.GET("/", handler)
	ts.engine.ServeHTTP(ts.recoder, ts.getRootRequest())

	ts.Assert().Equal(500, ts.recoder.Code)
	var payload map[string]interface{}
	json.NewDecoder(ts.recoder.Body).Decode(&payload)
	ts.Assert().Equal("plain text panic", payload["error"].(string))
}

func TestRecover(t *testing.T) {
	suite.Run(t, new(recoverTestSuite))
}
