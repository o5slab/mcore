package gina

import (
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// RequestLog ...
func RequestLog(serviceName string) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery
		if raw != "" {
			path = path + "?" + raw
		}

		c.Next()

		end := time.Now()
		latency := end.Sub(start)
		logrus.
			WithField("status", c.Writer.Status()).
			WithField("method", c.Request.Method).
			WithField("path", path).
			WithField("ip", c.ClientIP()).
			WithField("latency", latency.String()).
			WithField("user-agent", c.Request.UserAgent()).
			WithField("service", serviceName).
			WithField("x-request-id", c.GetString("x-request-id")).
			WithField("upstream", c.GetString("upstream")).
			WithField("proto", c.Request.Proto).
			Info("request_log")
	}
}
