package gina

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestError(t *testing.T) {
	rerr := NewResponseError(400, "test error")
	assert.Equal(t, 400, rerr.GetStatusCode())
	assert.Equal(t, "test error", rerr.Error())

	// should response an code describing on empty message
	assert.Equal(t, "error with status 400", NewResponseError(400, "").Error())

	// empty map should return a single key error
	assert.Equal(t, "test error", NewResponseError(400, "test error").GetErrorsMap()["error"])
}

func TestErrorMap(t *testing.T) {
	rerr := NewResponseError(400, "test error")
	rerr.AddError("username", "must not be empty")

	errmap := rerr.GetErrorsMap()
	assert.Equal(t, "must not be empty", errmap["username"])
}
