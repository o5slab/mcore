package gina

import (
	"fmt"
)

// ResponseError presents an error that can respond to client via http structure
type ResponseError interface {
	error

	GetStatusCode() int
	GetErrorsMap() map[string]string
	AddError(key, value string) ResponseError
}

type responseErrorImpl struct {
	msg       string
	code      int
	errorsMap map[string]string
}

// AddError ...
func (r *responseErrorImpl) AddError(key, value string) ResponseError {
	r.errorsMap[key] = value
	return r
}

// Error ...
func (r *responseErrorImpl) Error() string {
	if r.msg == "" {
		return fmt.Sprintf("error with status %d", r.code)
	}
	return r.msg
}

func (r *responseErrorImpl) GetStatusCode() int {
	return r.code
}

// GetErrorsMap ...
func (r *responseErrorImpl) GetErrorsMap() map[string]string {
	// in case there is no item in map, take the message to response to client
	if len(r.errorsMap) == 0 {
		r.errorsMap["error"] = r.Error()
	}
	return r.errorsMap
}

// NewResponseError ...
func NewResponseError(code int, msg string) ResponseError {
	return &responseErrorImpl{
		msg:       msg,
		code:      code,
		errorsMap: map[string]string{},
	}
}
