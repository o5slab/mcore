#!/bin/bash

covfile='coverage.txt'
echo 'mode: atomic' > "$covfile"

packages=$(go list -a ./...)

# exit on error
set -e
for package in ${packages[@]}; do
	go test -coverprofile=profile.out -covermode=atomic "$package"
	if [[ -f profile.out ]]; then
		tail -n +2 profile.out >> "$covfile"
		rm -f profile.out
	fi
done

go tool cover -func "$covfile"
